const express = require("express");
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.set("port", process.env.PORT || 3001);

var assert = require('assert');

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/collect');
var Game;
mongoose.connection.once('open', function() {
  Game = require('./game.js');
});

app.listen(3001, function() {
  console.log('API listening on port 3001.\n');
});

app.get('/games', function(req, res) {
  Game.find(function(err, games) {
    if (err) return console.error(err);
    
    console.log("Retrieving games... ");
    console.log("Found " + games.length + " games.\n");
    res.send(games);
  });
});

app.post('/games', function(req, res) {
  getNextGameID(function(next_id) {
    req.body['_id'] = next_id;
    var newGame = new Game(req.body);

    newGame.save(function(err, game) {
      if (err) {
        console.log(err);
        res.statusCode = 500;
        return res.json({
          errors: ['Could not add game']
        });
      }

      console.log("Adding: ");
      console.log(req.body);
      console.log("");

      res.statusCode = 201;
      res.json(req.body);
    });
  });
});

/* app.get('/games/:gameId', function(req, res) {
  db.collection('games').find({'_id':5}).toArray(function(err, resuts){
    console.log(results);
    res.send(results);  
  });
}); */

app.delete('/games/:id', function(req, res) {
  Game.findByIdAndRemove(parseInt(req.params.id), function (err, result) {
    if (err) return res.send(500, err);

    console.log("Deleting: " + result + "\n");

    res.sendStatus(200);
  });
});

function getNextGameID(callback) {
  Game.find().sort({ '_id': -1 }).limit(1).exec(function(err, result) {
    if(err) {
      console.log(err);
      return err;
    }

    if (result.length < 1) {
      callback(0);
      return;
    } 

    var max_id = result[0]._id;
    callback(max_id + 1);
  });
}
