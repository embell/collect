var http = require('http');
var fs = require('fs');
var path = require('path');

var seeds1 = JSON.parse(fs.readFileSync('seeds1.json', 'utf-8'));
var seeds2 = JSON.parse(fs.readFileSync('seeds2.json', 'utf-8'));

var games = seeds1.concat(seeds2);

var i = 0;

addGames();

function addGames() {
  var body = JSON.stringify(games[i]);
  var options = {
    host: 'localhost',
    path: '/games/',
    port: '3001',
    method: 'POST',
    headers: { 'Content-Type': 'application/json', 
               'Content-Length': body.length,},
  };

  callback = function(response) {
    var str = '';

    //another chunk of data has been recieved, so append it to `str`
    response.on('data', function (chunk) {
      str += chunk;
    });

    //the whole response has been recieved, so we just print it out here
    response.on('end', function () {
      console.log(str);
      i = i + 1;
      if (i < games.length) {
        console.log("Next");
        addGames();
      }
    });
  }

  http.request(options, callback).write(body);
}
