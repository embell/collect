import React from 'react';
import GameInfo from './GameInfo';
import Utils from './Utils';

import './css/Game.css';

class Game extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {
      showInfo: false,
    };
  }

  render() {
    var game = this.props.game;
   
    return (
      <li className="Game">
        <h3 onClick={() => this.setState({showInfo: !this.state.showInfo})} 
            className={this.state.showInfo ? "game-title-bar active" : "game-title-bar"}>
          <div className="game-system">
            {game.system} 
          </div>
          <div className="game-title">
            {game.title}
          </div>
          { game.release !== null &&
            <div className="game-release">
              {Utils.formattedDateString(new Date(game.release))}
            </div>
          }
        </h3>

        {this.state.showInfo &&
          <GameInfo game={this.props.game} page={this.props.page} refresh={this.props.refresh} />
        }
      </li>
    );
  }
}

export default Game;
