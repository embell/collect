import React from 'react';
import Client from './Client';
import Utils from './Utils';

import './css/GameInfo.css';
import Times from 'react-icons/lib/ti/times';
import Pencil from 'react-icons/lib/ti/edit';
import Check from 'react-icons/lib/ti/tick';

class GameInfo extends React.Component {
  constructor(props) {
    super(props);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleDelete(id) {
    Client.deleteGame(this.props.game._id, this.props.refresh); 
  }

  render() {
    var release = this.props.game.release;

    return (
      <div className="GameInfo">
        { this.props.page === 'wishlist' && 
          <button className="move-to-collection-button" 
                  onClick={() => console.log("Want to move to wishlist.")}>
            <Check size={16}/>
          </button>
        }

        <button className="edit-button" 
                onClick={() => console.log("Want to edit.")}>
          <Pencil size={16}/>
        </button>
        <button className="delete-button" 
                onClick={() => this.handleDelete()}>
          <Times size={16}/>
        </button>

        <p className="game-progress">{this.props.game.progress}</p>
        <p className="game-review">{this.props.game.review}</p>
        <p className="game-condition">{this.props.game.condition}</p>
        <div className="clearfix"></div>
      </div>
    );
  }
}

export default GameInfo;
