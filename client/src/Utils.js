function formattedDateString(date) {
  if (isNaN(date.getFullYear())) {
    return "";
  }

  return (
    getFullMonth(date.getMonth()) + " " +
    date.getDate() + ", " + 
    date.getFullYear()
  );
}

function getFullMonth(monthNumber) {
  var monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
                'July', 'August', 'September', 'October', 'November', 'December'];

  return monthNames[monthNumber];
}

function getSystemList(games) {
  var systems = [];

  for (var i=0; i<games.length; i++) {
    if (!systems.includes(games[i].system)) {
      systems.push(games[i].system);
    } 
  }

  return systems.sort();
}

const Utils = { formattedDateString, getSystemList }
export default Utils;
