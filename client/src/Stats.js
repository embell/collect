import React from 'react';
import Client from './Client';

import './css/Stats.css'

import Times from 'react-icons/lib/md/close';

class Stats extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var collection, wishlist;
    var collectionByConsole;
    var wishlistByConsole;
    var console, count;
    
    //TODO: Possibly better to only loop through once instead of using filter function twice?
    collection = this.props.games.filter(function(g) {
      return !g.wishlist;
    });

    wishlist = this.props.games.filter(function(g) {
      return g.wishlist;
    });

    collectionByConsole = {};

    for (var i=0; i<collection.length; i++) {
      console = collection[i].system;
      count = collectionByConsole[console];

      if (typeof count === 'undefined') {
        collectionByConsole[console] = 1;
      } else {
        collectionByConsole[console] += 1;
      }
    }

    wishlistByConsole = {};

    for (var i=0; i<wishlist.length; i++) {
      console = wishlist[i].system;
      count = wishlistByConsole[console];

      if (typeof count === 'undefined') {
        wishlistByConsole[console] = 1;
      } else {
        wishlistByConsole[console] += 1;
      }
    }

    return (
      <div className="Stats">
        <div className="stat-table-grid">

          <div>
            <table>
              <caption className="table-title">Collection</caption>
              <thead>
                <tr>
                  <th>Console</th>
                  <th>Total Games</th>
                </tr>
              </thead>

              <tfoot>
                <tr>
                  <td>Everything</td>
                  <td>{collection.length}</td>
                </tr>
              </tfoot>

              {Object.keys(collectionByConsole).sort().map( console =>
                <tr>
                  <td>{console}</td>
                  <td>{collectionByConsole[console]}</td>
                </tr>
              )}
            </table>
          </div>

          <div>
            <table>
              <caption className="table-title">Wishlist</caption>
              <thead>
                <tr>
                  <th>Console</th>
                  <th>Total Games</th>
                </tr>
              </thead>

              <tfoot>
                <tr>
                  <td>Everything</td>
                  <td>{wishlist.length}</td>
                </tr>
              </tfoot>

              {Object.keys(wishlistByConsole).sort().map( console =>
                <tr>
                  <td>{console}</td>
                  <td>{wishlistByConsole[console]}</td>
                </tr>
              )}
            </table>
          </div>

        </div>
      </div>
    );
  }
}

export default Stats;
