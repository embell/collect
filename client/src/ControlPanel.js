import React, { Component } from 'react';

import AddGameForm from './AddGameForm';
import SortForm from './SortForm';
import FilterForm from './FilterForm';
import './css/ControlPanel.css';

import Plus from 'react-icons/lib/ti/plus';
import Minus from 'react-icons/lib/ti/minus';

class ControlPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expansion: 'none',
    };
  }

  toggleExpandedSection(section) {
    if (this.state.expansion == section) {
      this.setState({
        expansion: 'none',
      });
    } else {
      this.setState({
        expansion: section,
      });
    }
  }

  getIcon(section) {
    var iconSize = 20;
    if (this.state.expansion == section) {
      return(<Minus size={iconSize} />);
    } else {
      return(<Plus size={iconSize} />);
    }
  }

  render() {
    return(
      <div className={this.props.page === 'wishlist' ? 'ControlPanel wishlist' : 'ControlPanel'}>
        <div className="container">
          <div className="control-grid">
            <div className="control-item">
              <button className={this.state.expansion == 'add' ? 'active' : ''}
                      onClick={() => this.toggleExpandedSection('add')}> 
                Add Game 
                {this.getIcon('add')}
              </button> 
            </div>

            <div className="control-item">
              <button className={this.state.expansion == 'sort' ? 'active' : ''}
                      onClick={() => this.toggleExpandedSection('sort')}> 
                Sort
                {this.getIcon('sort')}
              </button> 
            </div>

            <div className="control-item">
              <button className={this.state.expansion == 'filter' ? 'active' : ''}
                      onClick={() => this.toggleExpandedSection('filter')}> 
                Filter
                {this.getIcon('filter')}
              </button> 
            </div>

            <div className="control-item">
              <button className={this.state.expansion == 'search' ? 'active' : ''}
                      onClick={() => this.toggleExpandedSection('search')}> 
                Search
                {this.getIcon('search')}
              </button> 
            </div>
          </div>
          
          {this.state.expansion != 'none' &&
            <hr />
          }

          {this.state.expansion == 'add' && 
            <AddGameForm page={this.props.page} refresh={this.props.refresh} />
          }

          {this.state.expansion == 'sort' && 
            <SortForm sortMode={this.props.sortMode} changeSort={this.props.changeSort}/>
          }

          {this.state.expansion == 'filter' && 
            <FilterForm games={this.props.games} filters={this.props.filters}
                        changeFilter={this.props.changeFilter} clearFilter={this.props.clearFilter} />
          }

          {this.state.expansion == 'search' && 
            <div>
              <h3>Search</h3>
              <form><input></input></form>
            </div>
          }
        </div>
      </div>
    );
  }
}

export default ControlPanel;
