import React from 'react';
import Client from './Client';

import './css/SortForm.css';

class SortForm extends React.Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);

    this.state = {
      order: 'ascending',
    };
  }

  handleChange(event) {
    this.props.changeSort(event.target.value);
  }

  render() {
    return (
      <div className="SortForm">
        <h3>Sort By</h3>
          <div className="sort-form-grid">
            <button className={"radio-option" + (this.props.sortMode == 'system' ? ' active' : '')} 
                    value="system"
                    onClick={this.handleChange}>
              System 
            </button>

            <button className={"radio-option" + (this.props.sortMode == 'title' ? ' active' : '')} 
                    value="title"
                    onClick={this.handleChange}>
              Title 
            </button>

            <button className={"radio-option" + (this.props.sortMode == 'release' ? ' active' : '')} 
                    value="release"
                    onClick={this.handleChange}>
              Release Date 
            </button>

            <button className={"radio-option" + (this.props.sortMode == 'dateAdded' ? ' active' : '')} 
                    value="dateAdded"
                    onClick={this.handleChange}>
              Date Added 
            </button>
          </div>
      </div>
    );
  }
}

export default SortForm;
