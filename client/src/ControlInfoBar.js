import React from 'react';
import Client from './Client';
import Game from './Game';

import './css/ControlInfoBar.css'

import SortIcon from 'react-icons/lib/ti/arrow-unsorted';
import FilterIcon from 'react-icons/lib/ti/filter';

class ControlInfoBar extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var sortMode, order;

    sortMode = this.props.sortMode;

    if (sortMode == 'system' || sortMode == 'title') {
      order = 'ascending';
    } else {
      order = 'descending';
    }

    if (sortMode == 'dateAdded') {
      sortMode = 'recently added';
    }

    return (
      <div className="ControlInfoBar">
        <div>
          <div className="sort-info">
            <SortIcon size='20' />
            {sortMode.charAt(0).toUpperCase() + sortMode.slice(1) + ", " + order}
          </div>
          
          {Object.keys(this.props.filters).map( filterName =>
            <div className="filter-info">
              <FilterIcon size='20' />
              {filterName.charAt(0).toUpperCase() + filterName.slice(1) + "=" + this.props.filters[filterName]}
            </div>
          )}
        </div>

        <div className="result-count">
          {this.props.resultCount} games
        </div>
      </div>
    );
  }
}

export default ControlInfoBar;
