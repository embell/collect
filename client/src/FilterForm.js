import React from 'react';
import Utils from './Utils';

import './css/FilterForm.css';

class FilterForm extends React.Component {
  constructor(props) {
    super(props);

    this.handleSystemClick = this.handleSystemClick.bind(this);
    this.handleClear = this.handleClear.bind(this);
  }

  handleSystemClick(e) {
    this.props.changeFilter('system', e.target.value); 
  }

  handleClear() {
    this.props.clearFilter();
  }

  render() {
    var systems = Utils.getSystemList(this.props.games);
    return (
      <div className="FilterForm">
        <h3>Filter</h3>
          <div className="system-filter-grid">
            {systems.map( system =>
              <button className={"radio-option" + (this.props.filters['system'] == system ? ' active' : '')} 
                      key={system} value={system} onClick={this.handleSystemClick}>
                {system}
              </button>
            )}
          </div>
          <button className="clear-button" onClick={this.handleClear}>
            Clear
          </button>
      </div>
    );
  }
}

export default FilterForm;
