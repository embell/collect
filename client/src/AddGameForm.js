import React from 'react';
import Client from './Client';

import './css/AddGameForm.css';

class AddGameForm extends React.Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.showResult = this.showResult.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      title: '',
      system: '',
      release: '',
      progress: '',
      review: '',
      condition: '',

      showResult: false,
      resultStatus: '',
      result: '',
    };
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  showResult(result) {
    var resultStatus;
    var resultMessage;

    if (result.status >= 200 && result.status < 300) {
      resultStatus = 'success';
      resultMessage = 'Successfully added game';
    } else {
      resultStatus = 'failure';
      resultMessage = 'Failed to add game';
    }

    this.setState({result: resultMessage, resultStatus: resultStatus, showResult: true});
    this.props.refresh(); 
  }

  handleSubmit(event) {
    event.preventDefault();

    var wishlist = this.props.page === 'wishlist' ? true : false;

    Client.addGame({
      title: this.state.title,
      system: this.state.system,
      release: this.state.release,
      progress: this.state.progress,
      review: this.state.review,
      condition: this.state.condition,
      wishlist: wishlist,
    }, this.showResult);

    this.setState({
      title: '',
      system: '',
      release: '',
      progress: '',
      review: '',
      condition: '',
    });

    this.firstField.focus();
  }

  render() {
    return (
      <div>
        <h3>
          {this.props.page == 'wishlist' ? 'Add Game to Wishlist' : 'Add Game'} 
        </h3>

        <form onSubmit={this.handleSubmit}>
          <div className="add-form-grid">
            <div className="title-entry">
              <label>Title</label>
              <input type="text" name="title" ref={input => this.firstField = input}
                     value={this.state.title} 
                     onChange={this.handleChange}></input>
            </div>

            <div className="system-entry">
              <label>System</label>
              <input type="text" name="system"
                     value={this.state.system} 
                     onChange={this.handleChange}></input>
            </div>           

            <div className="release-date-entry">
              <label>Release Date</label>
              <input type="text" name="release" 
                     value={this.state.release} 
                     onChange={this.handleChange}></input>
            </div>

            {this.props.page !== 'wishlist' &&
              <div className="completion-note-entry">
                <label>Progress Note</label>
                <textarea name="progress" 
                          value={this.state.progress} 
                          onChange={this.handleChange}></textarea>
              </div>
            }

            {this.props.page !== 'wishlist' &&
              <div className="review-note-entry">
                <label>Review</label>
                <textarea name="review" 
                          value={this.state.review} 
                          onChange={this.handleChange}></textarea>
              </div>
            }

            {this.props.page !== 'wishlist' &&
              <div className="condition-note-entry">
                <label>Condition Note</label>
                <textarea name="condition" 
                          value={this.state.condition} 
                          onChange={this.handleChange}></textarea>
              </div>
            }
          </div>
          <input type="submit" value="Add"></input>
          {this.state.showResult && 
            <div className={"add-result " + this.state.resultStatus}>{this.state.result}</div>
          }
        </form>
      </div>
    );
  }
}

export default AddGameForm;
