function getGames() {
  return fetch('/games', {
    accept: "application/json"
  }).then(checkStatus)
    .then(parseJSON);
}

function addGame(gameObject, callback) {
  return fetch('/games', {
    method: 'POST',
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(gameObject),
  }).then(function(response) {
    callback(response);
  });
}

function deleteGame(id, callback) {
  return fetch('/games/' + id, {
    method: 'DELETE',
  }).then(function() {
    callback();
  });
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(`HTTP Error ${response.statusText}`);
  error.status = response.statusText;
  error.response = response;
  console.log(error); // eslint-disable-line no-console
  throw error;
}

function parseJSON(response) {
  return response.json();
}

const Client = { getGames, addGame, deleteGame };
export default Client;
