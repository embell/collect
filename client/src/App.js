import React, { Component } from 'react';

import ControlPanel from './ControlPanel';
import GameList from './GameList';
import Stats from './Stats';
import Client from './Client'

import List from 'react-icons/lib/ti/th-list';
import ListOutline from 'react-icons/lib/ti/th-list-outline';
import Chart from 'react-icons/lib/ti/chart-line';

import './css/App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      games: [],
      page: 'collection',
      sortMode: 'title',
      filters: {}
    };

    this.changeSort = this.changeSort.bind(this);
    this.changeFilter = this.changeFilter.bind(this);
    this.clearFilter = this.clearFilter.bind(this);
    this.applyFilters = this.applyFilters.bind(this);
    this.compareGames = this.compareGames.bind(this);
    this.updateGames = this.updateGames.bind(this);
    this.updateGames();
  }

  changeSort(s) {
    this.setState({
      sortMode: s,
    });
    this.updateGames();
  }

  compareGames(a, b) {
    var mainCompare
    if (this.state.sortMode === 'system') {
      mainCompare = a.system.localeCompare(b.system);

    } else if (this.state.sortMode === 'title') {
      mainCompare =  a.title.localeCompare(b.title);

    } else if (this.state.sortMode === 'release') {
      mainCompare =  new Date(b.release) - new Date(a.release); 

    } else if (this.state.sortMode === 'dateAdded') {
      mainCompare =  new Date(b.dateAdded) - new Date(a.dateAdded); 

    } else {
      mainCompare = a.title.localeCompare(b.title);
    }

    //If both have the same system, sort by title next
    if (mainCompare == 0 && this.state.sortMode != 'title') {
      return a.title.localeCompare(b.title);
    //Unless title is already the main comparison, then use release as secondary
    } else if (mainCompare == 0) {
      return new Date(a.release) - new Date(b.release); 
    } else {
      return mainCompare;
    }
  }

  changeFilter(category, value) {
    var newFilters = this.state.filters;
    newFilters[category] = value;
    this.setState({
      filters: newFilters
    });
  }

  clearFilter() {
    this.setState({
      filters: {}
    });
  }

  applyFilters() {
    var filteredList = this.state.games;

    var filters = Object.entries(this.state.filters);
    var key = 0;
    var value = 1;

    for (var i=0; i<filters.length; i++) {
      filteredList = filteredList.filter(function(g) {
        return (g[filters[i][key]] == filters[i][value]);
      });
    } 

    return filteredList;
  }

  updateGames() {
    var app = this;
    Client.getGames().then(function(result) {
      app.setState({
        games: result.sort(app.compareGames),
      });
    });
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <div className="container header-container">
            <h1>Collect</h1>
            <ul className="nav">
              <li className={this.state.page == 'collection' ? 'active' : ''}
                  onClick={() => this.setState({page: 'collection'})}>
                <a>Collection <List /></a>
              </li>
              <li className={this.state.page == 'wishlist' ? 'active' : ''}
                  onClick={() => this.setState({page: 'wishlist'})}>
                <a>Wishlist <ListOutline /></a>
              </li>
              <li className={this.state.page == 'stats' ? 'active' : ''}
                  onClick={() => this.setState({page: 'stats'})}>
                <a>Stats <Chart /></a>
              </li>
            </ul>
          </div>
        </div>

        {(this.state.page == 'collection' || this.state.page == 'wishlist') &&
          <ControlPanel games={this.state.games} page={this.state.page} refresh={this.updateGames} 
                        sortMode={this.state.sortMode} changeSort={this.changeSort}
                        filters={this.state.filters} changeFilter={this.changeFilter}
                        clearFilter={this.clearFilter}/>
        }


        <div className="main container">

          {this.state.page === 'collection' &&
            <GameList 
              games={this.applyFilters().filter((g) => g.wishlist == false)} 
              page={this.state.page} refresh={this.updateGames} 
              sortMode={this.state.sortMode} filters={this.state.filters}
            />
          }

          {this.state.page === 'wishlist' &&
            <GameList 
              games={this.applyFilters().filter((g) => g.wishlist == true)} 
              page={this.state.page} refresh={this.updateGames} 
              sortMode={this.state.sortMode} filters={this.state.filters}
            />
          }

          {this.state.page === 'stats' &&
            <Stats games={this.state.games}/>
          }
        </div>
      </div>
    );
  }
}

export default App;
