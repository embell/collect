import React from 'react';
import ControlInfoBar from './ControlInfoBar';
import Game from './Game';

import './css/GameList.css'

class GameList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="GameList">
        <ControlInfoBar sortMode={this.props.sortMode} filters={this.props.filters} 
                        resultCount={this.props.games.length}/>

        <ul className={this.props.page === 'wishlist' ? "game-list wishlist" : "game-list"}>
          {this.props.games.map( game =>
            <Game game={game} key={game._id} refresh={this.props.refresh} page={this.props.page}/>
          )}
        </ul>
      </div>
    );
  }
}

export default GameList;
