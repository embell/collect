var mongoose = require('mongoose');

var GameSchema = mongoose.Schema({
  _id: {
    type: Number,
    required: true,
  },

  title: { 
    type: String, 
    required: true,
  },

  system: { 
    type: String, 
    required: true,
  },

  release: { 
    type: Date, 
  },

  progress: { 
    type: String, 
  },

  review: { 
    type: String, 
  },

  condition: { 
    type: String, 
  },

  wishlist: {
    type: Boolean,
    required: true,
    default: false,
  },

  dateAdded: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model('Game', GameSchema);
